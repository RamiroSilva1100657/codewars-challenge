package com.cocus.challenge.codewars.ui.challenge;

import com.cocus.challenge.codewars.API.CodeWarsService;
import com.cocus.challenge.codewars.model.CodeChallengeResponse;

import android.util.Log;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ChallengeDetailsPresenter {

    private ChallengeDetailsPresenterInterface mChallengeDetailsPresenterInterface;

    public ChallengeDetailsPresenter(ChallengeDetailsPresenterInterface challengeDetailsPresenterInterface) {
        mChallengeDetailsPresenterInterface = challengeDetailsPresenterInterface;
    }

    public void doCodeChallengeSearch(String id) {
        CodeWarsService.getInstance().getCodeChallenge(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CodeChallengeResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(CodeChallengeResponse codeChallengeResponse) {
                        mChallengeDetailsPresenterInterface.onChallengeDetailsLoaded(codeChallengeResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("CHALLENGE DETAILS", e.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
