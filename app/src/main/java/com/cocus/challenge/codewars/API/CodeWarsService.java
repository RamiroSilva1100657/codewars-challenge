package com.cocus.challenge.codewars.API;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.cocus.challenge.codewars.model.AuthoredChallengesResponse;
import com.cocus.challenge.codewars.model.CodeChallengeResponse;
import com.cocus.challenge.codewars.model.CompletedChallengesResponse;
import com.cocus.challenge.codewars.model.UserResponse;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public class CodeWarsService {

    private static final String BASE_URL = "https://www.codewars.com/api/v1/";
    private static CodeWarsService mInstance;
    private ICodeWarsService mCodeWarsService;

    public static CodeWarsService getInstance() {
        if (mInstance == null) {
            mInstance = new CodeWarsService();
        }

        return mInstance;
    }

    private CodeWarsService() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        OkHttpClient okHttpClient = builder.build();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();

        mCodeWarsService = retrofit.create(ICodeWarsService.class);
    }

    public Observable<UserResponse> getUser(String idOrUsername) {
        return mCodeWarsService.getUser(idOrUsername);
    }

    public Observable<CompletedChallengesResponse> getCompletedChallenges(String idOrUsername, int page) {
        return mCodeWarsService.getCompletedChallenges(idOrUsername, page);
    }

    public Observable<AuthoredChallengesResponse> getAuthoredChallenges(String idOrUsername) {
        return mCodeWarsService.getAuthoredChallenges(idOrUsername);
    }

    public Observable<CodeChallengeResponse> getCodeChallenge(String idOrSlug) {
        return mCodeWarsService.getCodeChallenge(idOrSlug);
    }

    private interface ICodeWarsService {

        /**
         * Request to search for the information of a specific user.
         *
         * @param idOrUsername of the user to search for.
         * @return The response for this specific user.
         */
        @GET("users/{idOrUsername}")
        Observable<UserResponse> getUser(@Path("idOrUsername") String idOrUsername);

        /**
         * Request to show the completed challenges of a certain user.
         *
         * @param idOrUsername of the user to show information of.
         * @param page         number of the page to show.
         * @return The response with the completed challenges.
         */
        @GET("users/{idOrUsername}/code-challenges/completed")
        Observable<CompletedChallengesResponse> getCompletedChallenges(@Path("idOrUsername") String idOrUsername, @Query("page") int page);

        /**
         * Request to show teh authored challenges of a certain user.
         *
         * @param idOrUsername of the user to show information of.
         * @return The response with the authored challenges.
         */
        @GET("users/{idOrUsername}/code-challenges/authored")
        Observable<AuthoredChallengesResponse> getAuthoredChallenges(@Path("idOrUsername") String idOrUsername);


        /**
         * Request to show the information of a certain code challenge.
         *
         * @param idOrSlug of the code challenge to present information of.
         * @return The response with the code challenge information.
         */
        @GET("code-challenges/{idOrSlug}")
        Observable<CodeChallengeResponse> getCodeChallenge(@Path("idOrSlug") String idOrSlug);
    }
}
