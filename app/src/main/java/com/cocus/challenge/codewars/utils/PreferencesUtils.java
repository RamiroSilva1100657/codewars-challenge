package com.cocus.challenge.codewars.utils;

import com.google.gson.Gson;

import com.cocus.challenge.codewars.model.UserResponse;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PreferencesUtils {

    private static final String SEARCH_PREFERENCES = "general_prefs";
    private static final String RECENT_SEARCH_PREF = "recent_search_pref";
    private static final int RECENTS_LIMIT = 5;

    public static List<UserResponse> getRecentUsersSearched(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SEARCH_PREFERENCES, Context.MODE_PRIVATE);
        Set<String> mostRecents = new HashSet<>(sharedPreferences.getStringSet(RECENT_SEARCH_PREF, Collections.<String>emptySet()));

        List<UserResponse> recents = new ArrayList<>();

        Gson gson = new Gson();
        for (String userJson : mostRecents) {
            UserResponse userResponse = gson.fromJson(userJson, UserResponse.class);
            recents.add(userResponse);
        }

        return recents;
    }

    public static void addRecentSearch(Context context, UserResponse userResponse) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SEARCH_PREFERENCES, Context.MODE_PRIVATE);
        Set<String> mostRecents = new HashSet<>(sharedPreferences.getStringSet(RECENT_SEARCH_PREF, Collections.<String>emptySet()));

        Gson gson = new Gson();
        String userJson = gson.toJson(userResponse);

        if (!mostRecents.isEmpty() && mostRecents.size() == RECENTS_LIMIT) {
            mostRecents.remove(mostRecents.iterator().next());
        }

        mostRecents.add(userJson);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putStringSet(RECENT_SEARCH_PREF, mostRecents);
        editor.apply();
    }

}
