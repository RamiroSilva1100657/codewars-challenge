package com.cocus.challenge.codewars.ui.mainSearch;

import com.cocus.challenge.codewars.API.CodeWarsService;
import com.cocus.challenge.codewars.model.UserResponse;
import com.cocus.challenge.codewars.utils.PreferencesUtils;

import android.content.Context;
import android.util.Log;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainPresenter {

    public static final String TAG = "MainPresenter";

    private MainPresenterInterface mMainViewInterface;
    private Context mContext;

    public MainPresenter(MainPresenterInterface mainViewInterface, Context applicationContext) {
        this.mMainViewInterface = mainViewInterface;
        this.mContext = applicationContext;
    }

    public void doSearch(final String idOrUsername) {
        CodeWarsService.getInstance().getUser(idOrUsername)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(UserResponse userResponse) {
                        mMainViewInterface.onSearchResult(userResponse);
                        PreferencesUtils.addRecentSearch(mContext, userResponse);

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "Error: " + e.getMessage());
                        mMainViewInterface.showToast("Something went wrong when searching for the user "
                                + idOrUsername + ", or that user doesn't exist. Please try again");
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void loadRecentUserSearched() {
        mMainViewInterface.loadRecentUserSearched(PreferencesUtils.getRecentUsersSearched(mContext));
    }
}
