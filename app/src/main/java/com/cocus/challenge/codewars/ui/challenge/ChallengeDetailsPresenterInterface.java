package com.cocus.challenge.codewars.ui.challenge;

import com.cocus.challenge.codewars.model.CodeChallengeResponse;

public interface ChallengeDetailsPresenterInterface {

    void onChallengeDetailsLoaded(CodeChallengeResponse codeChallengeResponse);
}
