package com.cocus.challenge.codewars.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.util.Pair;

import java.util.Map;

public class Ranks {

    @SerializedName("languages")
    @Expose
    private Map<String, LanguageRank> languages;

    public Ranks() {
    }

    public Map<String, LanguageRank> getLanguages() {
        return languages;
    }

    public void setLanguages(Map<String, LanguageRank> languages) {
        this.languages = languages;
    }

    public Pair<String, Integer> getBestLanguage() {
        int highestScore = -1;
        Pair<String, Integer> bestLanguage = null;

        for (Map.Entry<String, LanguageRank> rankEntry : languages.entrySet()) {
            int languageScore = rankEntry.getValue().getScore();

            if (languageScore > highestScore) {
                highestScore = languageScore;
                bestLanguage = new Pair<>(rankEntry.getKey(), languageScore);
            }
        }

        return bestLanguage;
    }
}
