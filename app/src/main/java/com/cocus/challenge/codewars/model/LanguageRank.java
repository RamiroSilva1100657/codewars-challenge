package com.cocus.challenge.codewars.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LanguageRank {

    @SerializedName("score")
    @Expose
    private int score;

    public LanguageRank() {
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
