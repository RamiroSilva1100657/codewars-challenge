package com.cocus.challenge.codewars.ui.userChallenges;

import com.cocus.challenge.codewars.model.AuthoredChallenge;
import com.cocus.challenge.codewars.model.CompletedChallenge;

import java.util.List;

public interface UserChallengesPresenterInterface {
    void onCompletedChallengesLoaded(List<CompletedChallenge> completedChallenges);

    void onAuthoredChallengesLoaded(List<AuthoredChallenge> authoredChallenges);
}
