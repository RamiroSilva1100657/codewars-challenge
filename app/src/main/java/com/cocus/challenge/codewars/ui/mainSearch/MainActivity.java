package com.cocus.challenge.codewars.ui.mainSearch;

import com.cocus.challenge.codewars.R;
import com.cocus.challenge.codewars.model.UserResponse;
import com.cocus.challenge.codewars.ui.mainSearch.adapters.RecentUsersAdapter;
import com.cocus.challenge.codewars.ui.userChallenges.UserChallengesActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener,
        MainPresenterInterface,
        RecentUsersAdapter.ItemClickListener {

    MainPresenter mMainPresenter;
    RecentUsersAdapter mRecentUsersAdapter;

    @BindView(R.id.recent_users_container)
    RecyclerView recentUsersContainer;

    @BindView(R.id.layout_user_information)
    View userInformation;

    @BindView(R.id.user_name)
    TextView userName;

    @BindView(R.id.leaderboard_position)
    TextView leaderboardPosition;

    @BindView(R.id.best_user_language)
    TextView bestUserLanguage;

    @BindView(R.id.order_users_by_rank)
    TextView orderByRank;

    private static final String INTENT_EXTRA_USER_USERNAME = "intent.extra.user.username";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupMVP();

        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setupRecents();
    }

    private void setupRecents() {
        mMainPresenter.loadRecentUserSearched();
        orderByRank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRecentUsersAdapter.orderUsersByRank();
            }
        });
    }

    private void setupMVP() {
        mMainPresenter = new MainPresenter(this, getApplicationContext());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();

        menuInflater.inflate(R.menu.search_option, menu);
        MenuItem item = menu.findItem(R.id.search_button);

        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(this);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        mMainPresenter.doSearch(s);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }

    @Override
    public void showToast(String s) {
        Toast.makeText(MainActivity.this, s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSearchResult(final UserResponse userResponse) {
        userInformation.setVisibility(View.VISIBLE);
        userInformation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, UserChallengesActivity.class);
                intent.putExtra(INTENT_EXTRA_USER_USERNAME, userResponse.getUsername());
                startActivity(intent);
            }
        });
        userName.setText(TextUtils.isEmpty(userResponse.getName()) ? userResponse.getUsername() : userResponse.getName());
        leaderboardPosition.setText(String.valueOf(userResponse.getLeaderboardPosition()));
        bestUserLanguage.setText(String.format(Locale.getDefault(), "%s : %d pts", userResponse.getRanks().getBestLanguage().first, userResponse.getRanks().getBestLanguage().second));
    }

    @Override
    public void loadRecentUserSearched(List<UserResponse> recentUsersSearched) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recentUsersContainer.setLayoutManager(linearLayoutManager);

        mRecentUsersAdapter = new RecentUsersAdapter(recentUsersSearched, this);

        recentUsersContainer.setAdapter(mRecentUsersAdapter);
        recentUsersContainer.addItemDecoration(new DividerItemDecoration(this, linearLayoutManager.getOrientation()));
    }

    @Override
    public void onItemClickListener(UserResponse userResponse) {
        Intent intent = new Intent(this, UserChallengesActivity.class);
        intent.putExtra(INTENT_EXTRA_USER_USERNAME, userResponse.getUsername());
        startActivity(intent);
    }
}
