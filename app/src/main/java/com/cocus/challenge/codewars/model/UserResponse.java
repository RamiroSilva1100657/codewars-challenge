package com.cocus.challenge.codewars.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import android.support.annotation.NonNull;

public class UserResponse implements Comparable<UserResponse> {

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("leaderboardPosition")
    @Expose
    private int leaderboardPosition;

    @SerializedName("ranks")
    @Expose
    private Ranks ranks;

    public UserResponse() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLeaderboardPosition() {
        return leaderboardPosition;
    }

    public void setLeaderboardPosition(int leaderboardPosition) {
        this.leaderboardPosition = leaderboardPosition;
    }

    public Ranks getRanks() {
        return ranks;
    }

    public void setRanks(Ranks ranks) {
        this.ranks = ranks;
    }

    @Override
    public int compareTo(@NonNull UserResponse o) {
        return this.getLeaderboardPosition() - o.getLeaderboardPosition();
    }
}
