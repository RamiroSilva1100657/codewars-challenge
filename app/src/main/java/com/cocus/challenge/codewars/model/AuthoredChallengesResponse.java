package com.cocus.challenge.codewars.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AuthoredChallengesResponse {

    @SerializedName("data")
    @Expose
    private List<AuthoredChallenge> data;

    public AuthoredChallengesResponse() {
    }

    public List<AuthoredChallenge> getData() {
        return data;
    }

    public void setData(List<AuthoredChallenge> data) {
        this.data = data;
    }
}
