package com.cocus.challenge.codewars.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CompletedChallengesResponse {

    @SerializedName("totalPages")
    @Expose
    private int totalPages;

    @SerializedName("totalItems")
    @Expose
    private int totalItems;

    @SerializedName("data")
    @Expose
    private List<CompletedChallenge> data;

    public CompletedChallengesResponse() {
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(int totalItems) {
        this.totalItems = totalItems;
    }

    public List<CompletedChallenge> getData() {
        return data;
    }

    public void setData(List<CompletedChallenge> data) {
        this.data = data;
    }
}
