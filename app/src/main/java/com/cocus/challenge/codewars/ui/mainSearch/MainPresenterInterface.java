package com.cocus.challenge.codewars.ui.mainSearch;

import com.cocus.challenge.codewars.model.UserResponse;

import java.util.List;

public interface MainPresenterInterface {
    void loadRecentUserSearched(List<UserResponse> recentUsersSearched);

    void showToast(String s);

    void onSearchResult(UserResponse userResponse);
}

