package com.cocus.challenge.codewars.ui.userChallenges;

import com.cocus.challenge.codewars.API.CodeWarsService;
import com.cocus.challenge.codewars.model.AuthoredChallengesResponse;
import com.cocus.challenge.codewars.model.CompletedChallengesResponse;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class UserChallengesPresenter {

    private UserChallengesPresenterInterface mUserChallengesPresenterInterface;

    public UserChallengesPresenter(UserChallengesPresenterInterface userChallengesPresenterInterface) {
        mUserChallengesPresenterInterface = userChallengesPresenterInterface;
    }


    public void doCompletedChallengesSearch(String stringExtra, int i) {
        CodeWarsService.getInstance().getCompletedChallenges(stringExtra, i)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<CompletedChallengesResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(CompletedChallengesResponse completedChallengesResponse) {
                        mUserChallengesPresenterInterface.onCompletedChallengesLoaded(completedChallengesResponse.getData());
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void doAuthoredChallengesSearch(String stringExtra) {
        CodeWarsService.getInstance().getAuthoredChallenges(stringExtra)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<AuthoredChallengesResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(AuthoredChallengesResponse authoredChallengesResponse) {
                        mUserChallengesPresenterInterface.onAuthoredChallengesLoaded(authoredChallengesResponse.getData());
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
