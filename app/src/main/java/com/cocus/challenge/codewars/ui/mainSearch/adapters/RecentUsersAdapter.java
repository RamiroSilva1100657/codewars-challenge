package com.cocus.challenge.codewars.ui.mainSearch.adapters;

import com.cocus.challenge.codewars.R;
import com.cocus.challenge.codewars.model.UserResponse;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class RecentUsersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<UserResponse> mRecentUsers;
    private ItemClickListener mItemClickListener;

    public RecentUsersAdapter(List<UserResponse> recentUsers, ItemClickListener itemClickListener) {
        mRecentUsers = recentUsers;
        mItemClickListener = itemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View mView;
        TextView mUserName;
        TextView mLeaderboardPosition;
        TextView mUserBestLanguage;
        UserResponse mUserResponse;

        ViewHolder(@NonNull View view) {
            super(view);
            mView = view;
            mUserName = view.findViewById(R.id.user_name);
            mLeaderboardPosition = view.findViewById(R.id.user_leaderboard_position);
            mUserBestLanguage = view.findViewById(R.id.user_best_language);
            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mItemClickListener.onItemClickListener(mUserResponse);
                }
            });
        }
    }

    public void orderUsersByRank() {
        Collections.sort(mRecentUsers);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_recents_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof ViewHolder) {
            UserResponse userResponse = mRecentUsers.get(i);
            ((ViewHolder) viewHolder).mUserResponse = userResponse;
            ((ViewHolder) viewHolder).mUserName.setText(TextUtils.isEmpty(userResponse.getName()) ? userResponse.getUsername() : userResponse.getName());
            ((ViewHolder) viewHolder).mLeaderboardPosition.setText(String.valueOf(userResponse.getLeaderboardPosition()));
            ((ViewHolder) viewHolder).mUserBestLanguage.setText(String.format(Locale.getDefault(), "%s : %d pts", userResponse.getRanks().getBestLanguage().first, userResponse.getRanks().getBestLanguage().second));
        }
    }

    @Override
    public int getItemCount() {
        return mRecentUsers.size();
    }

    public interface ItemClickListener {
        void onItemClickListener(UserResponse userResponse);
    }
}
