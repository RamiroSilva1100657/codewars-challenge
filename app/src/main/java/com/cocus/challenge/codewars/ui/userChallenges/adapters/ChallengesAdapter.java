package com.cocus.challenge.codewars.ui.userChallenges.adapters;

import com.cocus.challenge.codewars.R;
import com.cocus.challenge.codewars.model.AuthoredChallenge;
import com.cocus.challenge.codewars.model.CompletedChallenge;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class ChallengesAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<T> mChallenges;
    private ItemClickListener mItemClickListener;

    public ChallengesAdapter(List<T> challenges, ItemClickListener itemClickListener) {
        mChallenges = challenges;
        mItemClickListener = itemClickListener;
    }


    public class ViewHolder<T> extends RecyclerView.ViewHolder {
        View mView;
        TextView mTextView;
        T mChallenge;

        ViewHolder(@NonNull final View itemView) {
            super(itemView);
            mView = itemView;
            mTextView = itemView.findViewById(R.id.challenge);
            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mItemClickListener.onItemClickListener(mChallenge);
                }
            });
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.user_challenge_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof ViewHolder) {
            Object object = mChallenges.get(i);
            ((ViewHolder) viewHolder).mChallenge = object;
            if (object instanceof CompletedChallenge) {
                ((ViewHolder) viewHolder).mTextView.setText(((CompletedChallenge) object).getName());
            } else if (object instanceof AuthoredChallenge) {
                ((ViewHolder) viewHolder).mTextView.setText(((AuthoredChallenge) object).getName());
            }
        }
    }

    @Override
    public int getItemCount() {
        return mChallenges.size();
    }

    public interface ItemClickListener<T> {
        void onItemClickListener(T challenge);
    }
}
