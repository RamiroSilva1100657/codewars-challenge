package com.cocus.challenge.codewars.ui.userChallenges;

import com.cocus.challenge.codewars.R;
import com.cocus.challenge.codewars.model.AuthoredChallenge;
import com.cocus.challenge.codewars.model.CompletedChallenge;
import com.cocus.challenge.codewars.ui.challenge.ChallengeDetailsActivity;
import com.cocus.challenge.codewars.ui.userChallenges.adapters.ChallengesAdapter;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserChallengesActivity extends AppCompatActivity implements UserChallengesPresenterInterface, ChallengesAdapter.ItemClickListener {

    ChallengesAdapter mChallengesAdapter;

    @BindView(R.id.navigation)
    BottomNavigationView navigation;

    @BindView(R.id.challenge_content)
    RecyclerView challengesList;

    private UserChallengesPresenter mUserChallengesPresenter;

    private static final String INTENT_EXTRA_USER_USERNAME = "intent.extra.user.username";
    private static final String INTENT_EXTRA_CHALLENGE_ID = "intent.extra.challenge.id";


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.completed_challenges:
                    mUserChallengesPresenter.doCompletedChallengesSearch(getIntent().getStringExtra(INTENT_EXTRA_USER_USERNAME), 0);
                    return true;
                case R.id.authored_challenges:
                    mUserChallengesPresenter.doAuthoredChallengesSearch(getIntent().getStringExtra(INTENT_EXTRA_USER_USERNAME));
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_challenges);

        ButterKnife.bind(this);

        mUserChallengesPresenter = new UserChallengesPresenter(this);

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    @Override
    public void onCompletedChallengesLoaded(List<CompletedChallenge> completedChallenges) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        challengesList.setLayoutManager(linearLayoutManager);

        mChallengesAdapter = new ChallengesAdapter(completedChallenges, this);

        challengesList.setAdapter(mChallengesAdapter);
        mChallengesAdapter.notifyDataSetChanged();
        challengesList.addItemDecoration(new DividerItemDecoration(this, linearLayoutManager.getOrientation()));
    }

    @Override
    public void onAuthoredChallengesLoaded(List<AuthoredChallenge> authoredChallenges) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        challengesList.setLayoutManager(linearLayoutManager);

        mChallengesAdapter = new ChallengesAdapter(authoredChallenges, this);

        challengesList.setAdapter(mChallengesAdapter);
        mChallengesAdapter.notifyDataSetChanged();
        challengesList.addItemDecoration(new DividerItemDecoration(this, linearLayoutManager.getOrientation()));
    }

    @Override
    public void onItemClickListener(Object challenge) {
        Intent intent = new Intent(this, ChallengeDetailsActivity.class);
        if (challenge instanceof CompletedChallenge) {
            intent.putExtra(INTENT_EXTRA_CHALLENGE_ID, ((CompletedChallenge) challenge).getId());
        } else if (challenge instanceof AuthoredChallenge) {
            intent.putExtra(INTENT_EXTRA_CHALLENGE_ID, ((AuthoredChallenge) challenge).getId());
        }
        startActivity(intent);
    }
}
