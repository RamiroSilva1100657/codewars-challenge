package com.cocus.challenge.codewars.ui.challenge;

import com.cocus.challenge.codewars.R;
import com.cocus.challenge.codewars.model.CodeChallengeResponse;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChallengeDetailsActivity extends AppCompatActivity implements ChallengeDetailsPresenterInterface {

    @BindView(R.id.challenge_name)
    TextView challengeName;

    @BindView(R.id.challenge_description)
    TextView challengeDescription;

    private ChallengeDetailsPresenter mChallengeDetailsPresenter;

    private static final String INTENT_EXTRA_CHALLENGE_ID = "intent.extra.challenge.id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge_details);

        setTitle("Challenge details");

        ButterKnife.bind(this);

        mChallengeDetailsPresenter = new ChallengeDetailsPresenter(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mChallengeDetailsPresenter.doCodeChallengeSearch(getIntent().getStringExtra(INTENT_EXTRA_CHALLENGE_ID));
    }

    @Override
    public void onChallengeDetailsLoaded(CodeChallengeResponse codeChallengeResponse) {
        challengeName.setText(codeChallengeResponse.getName());
        challengeDescription.setText(TextUtils.isEmpty(codeChallengeResponse.getDescription()) ? "No description available" : codeChallengeResponse.getDescription());
    }
}
